const express = require("express");
const req = require("express/lib/request");
const {
    json
} = require("express/lib/response");
const res = require("express/lib/response");
const filesystem = require("fs")

const app = express();

const data = require("./heroMl.json")
app.use(express.json());

app.get("/", (req, res) => {
    res.json({
        success: true,
        status: 200,
        rowCount: data.length,
        message: "success",
        data: data
    })
    console.log("ini berhasil di jalankan");
})

app.get("/:idhero", (req, res) => {
    const idhero = req.params.idhero;
    const dataId = data.filter(item => item.hero_id === parseInt(idhero));

    if (dataId.length < 1) {
        res.status(400).json({
            success: false,
            status: 400,
            rowCount: 0,
            message: "di luar jangkauwan",
            data: dataId
        })
    } else {
        res.json({
            success: true,
            status: 200,
            rowCount: 1,
            message: "Success",
            data: dataId[0]
        })
    }
})

app.put("/:idhero", (req, res) => {
    const idhero = req.params.idhero;
    const dataId = data.filter(item => item.hero_id === parseInt(idhero));

    const newName = req.body.hero_name || dataId[0].hero_name;
    const newAvatar = req.body.hero_avatar || dataId[0].hero_avatar;
    const newRole = req.body.hero_role || dataId[0].hero_role;
    const newSpecially = req.body.hero_specially || dataId[0].hero_specially;


    if (dataId.length < 1) {
        res.status(400).json({
            success: false,
            status: 400,
            rowCount: 0,
            message: "di luar jangkauwan",
            data: dataId[0] || "kosong"
        })
    } else {
        dataId[0].hero_name = newName;
        dataId[0].hero_avatar = newAvatar;
        dataId[0].hero_role = newRole;
        dataId[0].hero_specially = newSpecially;

        filesystem.writeFileSync("./heroMl.json", JSON.stringify(data))

        res.json({
            message: "success",
            data: dataId[0]
        })
        console.log(dataId[0])
    }
})

app.post("/", (req, res) => {
    const id = data.length + 1;
    const name = req.body.hero_name;
    const avatar = req.body.hero_avatar;
    const role = req.body.hero_role;
    const specially = req.body.hero_specially;

    data.push({
        "hero_id": id,
        "hero_name": name,
        "hero_avatar": avatar,
        "hero_role": role,
        "hero_specially": specially
    })
    filesystem.writeFileSync("./heroMl.json", JSON.stringify(data))
    res.json({
        data: data
    })
})

app.delete("/:idhero", (req, res) => {
    const idhero = req.params.idhero;
    const dataId = data.filter(item => {
        return parseInt(item.hero_id) !== parseInt(idhero)
    });

    res.json({
        data: dataId
    })


})




// app.delete("/", (req, res) => {
//     const idgame = req.body.id;
//     const newGame = daftarGame.filter(item => {
//         return parseInt(item.id) !== parseInt(idgame)
//     })
//     filesystem.writeFileSync("./game.json", JSON.stringify(newGame))

//     res.json({
//         message: "kamu menggunakan delete",
// data: newGame
// })
// })
console.log("ini Rian yang buat")

app.listen(6002, () => console.log("ini berjalan di port 6002 yang di buat oleh Rian"));

// "hero_id": 78,
// "hero_name": "Ruby",
// "hero_avatar": "https://dazelpro.com/files/images/ruby.jpg",
// "hero_role": "Fighter",
// "hero_specially": "Crowd Control"