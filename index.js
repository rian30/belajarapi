const express = require("express");
const filesystem = require("fs");

const app = express();
app.use(express.json());


const daftarGame = require("./game.json")

app.get("/", (req, res) => {
    const lihatData = req.query;
    const datagame = daftarGame.filter(user => {
        let idvalid = true;
        for (key in lihatData) {
            idvalid = idvalid && user[key] == lihatData[key];
        }
        return idvalid
    })
    res.json({
        message: "succsess",
        data: datagame[0]
    })
    // console.log(daftarGame)
})

// app.get("/", (req, res) => {
//     res.json({
//         message: "success",
//         data: daftarGame
//     })
// })
app.get("/:idgame", (req, res) => {
    const idgame = req.params.idgame;
    const datagame = daftarGame.filter(item => item.id === parseInt(idgame));
    if (datagame.length < 1) {
        res.status(400).json({
            message: "terjadi kesalahan di pencarian"
        })
    } else {
        res.json({
            message: "success",
            data: datagame[0]
        })

        console.log(datagame)
    }
})
// app.get("/:idgame?namagame", (req, res) => {
//     const namagame = req.params.namagame;
//     const datagame = daftarGame.filter(item => item.nama_game === parseInt(namagame));
//     console.log(datagame)

//     res.json({
//         message: "success",
//         data: datagame
//     })

// })

app.put("/:idgame", (req, res) => {
    const idgame = req.params.idgame;
    const datagame = daftarGame.filter(item => item.id === parseInt(idgame));

    const namagame = req.body.game || datagame[0].game;
    const devBaru = req.body.dev || datagame[0].dev;
    if (datagame.length < 1) {
        res.status(400).json({
            message: "ngotak lah"
        })
        console.log("gak ngotak")
    } else {
        datagame[0].game = namagame;
        datagame[0].dev = devBaru;
        res.json({
            message: "success",
            data: datagame[0]
        })

    }
    console.log(datagame)
})
console.log("ini berhasil")





app.post("/", (req, res) => {
    const id = daftarGame.length + 1;
    const game = req.body.game;
    const dev = req.body.dev;
    const by = req.body.by;

    daftarGame.push({
        "id": id,
        "game": game,
        "dev": dev,
        "by": by
    })
    filesystem.writeFileSync("./game.json", JSON.stringify(daftarGame))
    res.json({
        message: "succes",
        data: daftarGame
    })
})


app.delete("/", (req, res) => {
    const idgame = req.body.id;
    const newGame = daftarGame.filter(item => {
        return parseInt(item.id) !== parseInt(idgame)
    })
    filesystem.writeFileSync("./game.json", JSON.stringify(newGame))

    res.json({
        message: "kamu menggunakan delete",
        data: newGame
    })
})

app.listen(5001, () => console.log("ini saya buat"));